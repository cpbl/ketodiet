# I'm not sure what this repo is yet.  It's type-1-grit (ie advocates of low-carb solution to auto-immune diabetes) utilities and information, I suppose. Originally it was going to be Bernstein-diet recipe tools. 

 - I'm (re)-learning some [physiology](physiology.md)
 - compiling some [resources/ references](reading.md) for when I mention what I'm learning
 - insulin [injection site rotation code](injection_sites.py)
 - recipes and calculators?


# The plan:  Open-source ketogenic (oh, wait, no, not keto. the T1D diet gets more calories from protein than fat) diet nutritional information, cheat sheets, recipes, and calculation tools

Let's collaborate on a set of open-source resources for ketogenic (nutritional ketosis) (low-carb) diets.  

_My motivation is for Type-I (insulin dependent, i.e. autoimmune attack on insulin-producing cells) diabetes, or just for having a lower and more stable blood glucose and growing out of cravings,  but people also pursue keto diets to lose weight. I was vegetarian before this endeavor (i.e. wife's LADA diagnosis), but this project will include meat._

Goals
 - generate printable lists of carb content of various foods, but linked to the information sources each comes from
 - store recipes. The code will be able to scale them / calculate portions / and calculate nutritional amounts per serving of a recipe

 This requires a database of nutritional content. Many online are proprietary or awkward to copy. This repo will likely be coupled to a free and open google spreadsheet with the underlying data. 
  The code will likely work best by creating Python classes for each mass and volume unit, and for each type of food.  Those objects will know how to operate on each other.

Various of these features already have partial antecedents. E.g.:

a spreadsheet (carb only! no sources nor comments):
https://docs.google.com/spreadsheets/d/1CDDvA0aV_MoOeuYkpsPrLMklbwxnlNFV12w0pDhDTFE

Ah, the US FDA has a free database (now only as spreadsheet):
https://fdc.nal.usda.gov/download-datasets.html
But this is absurdly detailed and would need some serious trimming / extraction, not to mention parsing.  Nevertheless, it's the right source. Maybe there's already code to handle it.

This project needs help. Please join if you code or have recipes or etc.


