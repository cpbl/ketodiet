# Here are my questions
 - [x] How many ways do peripheral cells get energy from outside (fat, glucose? alcohol? ketones? wtf else?)  
    - [ ] What about the brain?
 - [ ] What's the difference between keto diet physiology and T1Dgrit diet physiology
 - [ ] What happens during exercise under different dietary and diabetic regimes
 - [ ] What causes cardiovascular disease?



# Story of ingested fat
 - In small intestine, bile salts emulsify fats; lipases break down trigs into fatty acids in order to transport across intestinal mucosa cell membrane
 - Inside the mucosa cell, fatty acids reassembled into trigs, so can be packaged for transported
 - trigs are packaged (where? inside mucosal cell and then delivered out by exocytosis?) into chylomicron (ie trigs, cholesterol, and (apolipo)proteins)
 - chylomicron is sent via lymph system to get dumped into blood stream (?)
 - when they get to a tissue that wants some, trigs split up again (lipase) into fatty acids in order to transport across tissue cell: how do they skip over the capillary epithelium?  tight junctions what?
 - fatty acids inside peripheral cell: here are some possibilities
     - myocyte (muscel cell): make ATP through beta oxidation, which means skip most of the Krebs cycle 
     - adipocite (fat cell): reassembled AGAIN into trig for storage
 - the chylomicron after doing a tour ends up at the liver, where the whole thing goes inside a cell (endocytosis) and hepatocytes do:
     - oxidize like myocytes, 
     - re-issue in VLDLs for transport again, 
     - or make ketones from them

# (Relevant) Liver functions
Liver has ~500 functions! Accounts for 20% of body's resting oxygen consumption!
### Carbohydrate metabolism
 - Glycogen: glycogenesis and glycogenolysis from/to glucose: ie stores about 100g of glycogen
   - also glyconeogenesis (glycogen) from lactic acid

 - Gluconeogenesis from amino acids, lactate, or glycerol.  Glycerol comes from liver and adipose cells.
 
 Adipose and liver cells produce glycerol by breakdown of fat, which the liver uses for gluconeogenesis.[41]

### Protein metabolism
 The liver makes virtually all our proteins and does (virtually all) the breaking down. Also synthesizes some amino acids

### Lipid metabolism
 - cholesterol synthesis, 
 - lipogenesis, 
 - production of triglycerides
 - and other stuff
 Who can hydrolize (split) a triglyceride into fatty acids? Fat cells do this. Anyone else?
 Who can burn fatty acids? Any cell! They get carved into sugar (?) and sent to the citric acid cycle.


# Fat cells (= adipocytes = lipocytes = adipose cells) 
  - white fat cells store one droplet of triglycerides & cholesteryl ester
  - secretes fatty acids (what about glycerol) 
  - secretes some proteins (anything relevant?)
  - triglyceride is made of 3 fatty acids and 1 glycerol:   
  - fatty acids arrive at a fat cell via chylomicron or VLDL but when delivered from fat cell, they are transported by albumin

# Ketones and ketone bodies
A ketone is just the R-CO2-R' molecular group (a double bond O), not a particular molecule. So that must be why we use the stupid term "ketone bodies" to describe simple sugars with this structure. A ketone body is a monosaccharide like sugar!! But we have a word for this, "ketose". !!! Why do we say ketone body, not ketose? 

Some examples of ketone bodies: acetone, beta-hydroxybutyrate, acetoacetate

# Ketosis
A metabolic state: elevated ketones ( = ketone bodies) in the blood. But with acid-base homeostasis.

Blood ketones increase 
When blood glucose reserves are low, the liver shifts from primarily metabolizing carbohydrates to metabolizing fatty acids.
When the liver rapidly metabolizes fatty acids into acetyl-CoA, some acetyl-CoA molecules can then be converted into ketone bodies: acetoacetate, beta-hydroxybutyrate, and acetone. These ketone bodies can function as an energy source (for peripheral cells, esp brain, not by liver cells) as well as signalling molecules.



# Ketoacidosis
Uncontrolled production of ketones in blood, upsetting pH.
"Usually the production of ketones is carefully controlled by several hormones, most importantly insulin."

A lack of insulin in the bloodstream allows unregulated fatty acid release from adipose tissue which increases fatty acid oxidation to acetyl CoA, some of which is diverted to ketogenesis. This raises ketone levels significantly above what is seen in normal physiology.

# Insulin (still go through wikipedia insulin page)

 - stops liver producing / releasing glucose
 - stops fat cells producing releasing fatty acids (and glycerol??)
 - tells fat cells to take up glucose and make fat
 - also?? tells fat cells to take up fatty acids
 - increases absorbtion of glucose from blood
 - It regulates the metabolism of carbohydrates, fats and protein by promoting the absorption of glucose from the blood into liver, fat and skeletal muscle cells.
 - In these tissues the absorbed glucose is converted into either glycogen  fats (triglycerides) via lipogenesis, or, in the case of the liver, into both
 - circulating insulin also affects the synthesis of proteins in a wide variety of tissues: it is the main anabolic hormone
 - reduces autophagy?

# Beta cells
- Beta cells are sensitive to (local to liver) blood sugar levels so that they secrete insulin into the blood in response to high level of glucose; 

# Different ways to make ATP / power muscles inside a mitochondrion
 - glucose: Krebs (citric acid) cycle normal route: 1 glucose -> 34 ATP
 - glucose to () and then skip citric acid cycle: very inefficient. "anaerobic respiration"  2 ATP??
 - fatty acid: beta oxidation: last bit of the Krebs cycle: 1 fatty acid -> 100 ATP
 - ketones: also get happily chomped by mitochondria: 1 acetoacetate -> 2 GTP + 22 ATP.  N.B.: Only mitochondria in heart, muscle, and brain can do this. Liver lacks the enzyme
 - alcohol: 3 steps to get to acetyl-coA, thence into Krebs cycle
 
 - Maybe there are many/countless other substances mitochondria can burn too?


In normal physiology, the heart uses mostly fatty acids, not glucose!





