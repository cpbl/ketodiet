#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__='cpbl'
import pandas as pd
import numpy as np
"""
This is a quick first draft, in case it catches anyone's attention as co-author/dev or interested users.
To do / help (PRs) desired:
  - use docopt
  - offer modes for multiple-injections per day
  - suggestion of body-conforming coordinate systems for the other zones: flank, butt, legs on the spreadsheets

Here're the definitions:
  https://docs.google.com/spreadsheets/d/1YuZrru11BwOKIcUUxfH1mDzm9kEITcKAiqXWDtMvW7M

"""
"""
Sample output:

 Current schedule goes until 2022-04-12 11:08:09.548401
                     site                                                                                 instructions
date                                                                                                                  
13-February-2022    Q:2,0  abdomen: quadrants: 2 finger widths looker's right of navel AND 0 finger widths above navel
14-February-2022   Q:6,-6  abdomen: quadrants: 6 finger widths looker's right of navel AND 6 finger widths BELOW navel
15-February-2022    LA:-2                                       arms: left arm: 2 finger widths BELOW centre of tricep
16-February-2022    Q:2,4  abdomen: quadrants: 2 finger widths looker's right of navel AND 4 finger widths above navel
17-February-2022   Q:-4,4   abdomen: quadrants: 4 finger widths looker's LEFT of navel AND 4 finger widths above navel
18-February-2022  Q:-6,-2   abdomen: quadrants: 6 finger widths looker's LEFT of navel AND 2 finger widths BELOW navel
19-February-2022    Q:4,4  abdomen: quadrants: 4 finger widths looker's right of navel AND 4 finger widths above navel
20-February-2022     LA:1                                       arms: left arm: 1 finger widths above centre of tricep
21-February-2022    Q:6,4  abdomen: quadrants: 6 finger widths looker's right of navel AND 4 finger widths above navel
22-February-2022    Q:0,4  abdomen: quadrants: 0 finger widths looker's right of navel AND 4 finger widths above navel
23-February-2022   Q:-6,2   abdomen: quadrants: 6 finger widths looker's LEFT of navel AND 2 finger widths above navel
24-February-2022   Q:-4,2   abdomen: quadrants: 4 finger widths looker's LEFT of navel AND 2 finger widths above navel
25-February-2022   Q:4,-4  abdomen: quadrants: 4 finger widths looker's right of navel AND 4 finger widths BELOW navel
26-February-2022    LA:-4                                       arms: left arm: 4 finger widths BELOW centre of tricep
27-February-2022    Q:4,2  abdomen: quadrants: 4 finger widths looker's right of navel AND 2 finger widths above navel
28-February-2022    LA:-3                                       arms: left arm: 3 finger widths BELOW centre of tricep
01-March-2022        LA:3                                       arms: left arm: 3 finger widths above centre of tricep
02-March-2022     Q:-4,-4   abdomen: quadrants: 4 finger widths looker's LEFT of navel AND 4 finger widths BELOW navel
03-March-2022     Q:-2,-2   abdomen: quadrants: 2 finger widths looker's LEFT of navel AND 2 finger widths BELOW navel
04-March-2022       Q:4,0  abdomen: quadrants: 4 finger widths looker's right of navel AND 0 finger widths above navel
05-March-2022        LA:4                                       arms: left arm: 4 finger widths above centre of tricep
06-March-2022      Q:4,-6  abdomen: quadrants: 4 finger widths looker's right of navel AND 6 finger widths BELOW navel
07-March-2022      Q:6,-2  abdomen: quadrants: 6 finger widths looker's right of navel AND 2 finger widths BELOW navel
08-March-2022        RA:2                                      arms: right arm: 2 finger widths above centre of tricep
09-March-2022      Q:0,-6  abdomen: quadrants: 0 finger widths looker's right of navel AND 6 finger widths BELOW navel
10-March-2022        RA:1                                      arms: right arm: 1 finger widths above centre of tricep
11-March-2022       Q:2,2  abdomen: quadrants: 2 finger widths looker's right of navel AND 2 finger widths above navel
12-March-2022      Q:-2,0   abdomen: quadrants: 2 finger widths looker's LEFT of navel AND 0 finger widths above navel
13-March-2022      Q:-6,4   abdomen: quadrants: 6 finger widths looker's LEFT of navel AND 4 finger widths above navel
14-March-2022      Q:4,-2  abdomen: quadrants: 4 finger widths looker's right of navel AND 2 finger widths BELOW navel
15-March-2022      Q:-2,2   abdomen: quadrants: 2 finger widths looker's LEFT of navel AND 2 finger widths above navel
16-March-2022       RA:-4                                      arms: right arm: 4 finger widths BELOW centre of tricep
17-March-2022      Q:0,-2  abdomen: quadrants: 0 finger widths looker's right of navel AND 2 finger widths BELOW navel
18-March-2022       RA:-3                                      arms: right arm: 3 finger widths BELOW centre of tricep
19-March-2022     Q:-4,-2   abdomen: quadrants: 4 finger widths looker's LEFT of navel AND 2 finger widths BELOW navel
20-March-2022     Q:-2,-4   abdomen: quadrants: 2 finger widths looker's LEFT of navel AND 4 finger widths BELOW navel
21-March-2022      Q:2,-2  abdomen: quadrants: 2 finger widths looker's right of navel AND 2 finger widths BELOW navel
22-March-2022      Q:-2,4   abdomen: quadrants: 2 finger widths looker's LEFT of navel AND 4 finger widths above navel
23-March-2022        RA:4                                      arms: right arm: 4 finger widths above centre of tricep
24-March-2022        RA:3                                      arms: right arm: 3 finger widths above centre of tricep
25-March-2022       RA:-2                                      arms: right arm: 2 finger widths BELOW centre of tricep
26-March-2022       Q:6,0  abdomen: quadrants: 6 finger widths looker's right of navel AND 0 finger widths above navel
27-March-2022     Q:-6,-4   abdomen: quadrants: 6 finger widths looker's LEFT of navel AND 4 finger widths BELOW navel
28-March-2022     Q:-2,-6   abdomen: quadrants: 2 finger widths looker's LEFT of navel AND 6 finger widths BELOW navel
29-March-2022        LA:0                                       arms: left arm: 0 finger widths above centre of tricep
30-March-2022      Q:2,-4  abdomen: quadrants: 2 finger widths looker's right of navel AND 4 finger widths BELOW navel
31-March-2022     Q:-4,-6   abdomen: quadrants: 4 finger widths looker's LEFT of navel AND 6 finger widths BELOW navel
01-April-2022      Q:2,-6  abdomen: quadrants: 2 finger widths looker's right of navel AND 6 finger widths BELOW navel
02-April-2022      Q:6,-4  abdomen: quadrants: 6 finger widths looker's right of navel AND 4 finger widths BELOW navel
03-April-2022       Q:0,2  abdomen: quadrants: 0 finger widths looker's right of navel AND 2 finger widths above navel
04-April-2022      Q:0,-4  abdomen: quadrants: 0 finger widths looker's right of navel AND 4 finger widths BELOW navel
05-April-2022      Q:-4,0   abdomen: quadrants: 4 finger widths looker's LEFT of navel AND 0 finger widths above navel
06-April-2022       Q:6,2  abdomen: quadrants: 6 finger widths looker's right of navel AND 2 finger widths above navel
07-April-2022        RA:0                                      arms: right arm: 0 finger widths above centre of tricep
08-April-2022      Q:-6,0   abdomen: quadrants: 6 finger widths looker's LEFT of navel AND 0 finger widths above navel
09-April-2022     Q:-6,-6   abdomen: quadrants: 6 finger widths looker's LEFT of navel AND 6 finger widths BELOW navel
10-April-2022       RA:-1                                      arms: right arm: 1 finger widths BELOW centre of tricep
11-April-2022        LA:2                                       arms: left arm: 2 finger widths above centre of tricep
12-April-2022       LA:-1                                       arms: left arm: 1 finger widths BELOW centre of tricep
"""


# from cpblUtilities.google_drive import downloadOpenGoogleDoc  This module has been copied into this distro, but its master version is in cpblUtilities distro (also GPL)
from google_drive import downloadOpenGoogleDoc

fn= downloadOpenGoogleDoc('https://docs.google.com/spreadsheets/d/1YuZrru11BwOKIcUUxfH1mDzm9kEITcKAiqXWDtMvW7M', filename='tmp_goodoc_regions_defs.ods',
                          fileformat='ods', pandas=False,
                          update=False)#True, )

df=pd.read_excel(fn,  engine='odf', skiprows=0, )
df=df.dropna(subset=['x'])
#for c in ('xmin','xmax','ymin','ymax'):
#    df[c]=df[c].astype(int)
knownSites=[]
if 0:
    excludedSites=  ['Q:{},{}'.format(x,y) for (x,y) in [(x,y) for x in range(-1,2) for y in range(-1,2)]]
    excludedSites=  ['Q:{},{}'.format(x,y) for x in range(-1,2) for y in range(-1,2)]
excludedSites = ['Q:0,0']
instructions={}
for ii,adf in df.iterrows():
    for x in str(adf['x']).split(','):
        for y in str(adf['y']).split(','):
            #print(adf['Code'],x,y)
            xd,yd= adf['x coordinate definition'], adf['y coordinate definition']
            ks='{}:{},{}'.format(adf['Code'],x,y)   if xd!='degenerate'    else  '{}:{}'.format(adf['Code'],y)
            if ks in excludedSites:
                print('Skipping '+ks)
                continue
            knownSites+=[ks]
            sx,sy=str(x),str(y)
            if int(x)<0:
                xd=xd.replace('above','BELOW').replace('right','LEFT')
                sx=str(-int(x))
            if int(y)<0:
                yd=yd.replace('above','BELOW').replace('right','LEFT')
                sy=str(-int(y))
            xins,yins = sx + ' '+ xd,     sy + ' '+ yd
            instructions[ks] = adf['Region']+': '+ adf['Zone']+ ': '+ (xd!='degenerate')*( xins + ' AND ')+ yins


print(' Generated {} allowed sites.'.format(len(knownSites)))
# Shuffle the list:

sched = pd.DataFrame(knownSites,columns=['site']).sample(frac=1, replace=False, random_state=0)
print(sched)

import pandas as pd
from datetime import datetime

datelist = pd.date_range(datetime.today(), periods=len(sched)).tolist()
sched['dt'] = pd.to_datetime(datelist)
sched['instructions'] = sched.site.map(lambda s: instructions[s])
print(' Current schedule goes until {}'.format(datelist[-1]))


sched['date'] = sched.dt.dt.strftime('%d-%B-%Y')
sched = sched.set_index('date')[['site','instructions']]
print(sched.to_string())
sched.to_csv('injection_schedule.tsv', sep='\t')


