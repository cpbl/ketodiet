# recipes

Loooooowww-carb recipes à la Dr Bernstein
and maybe others too
# Lemon Drop cookies
Ingredients
  
Lemon Cookies

    ▢ ¼ cup / 55g / 2 oz cream cheese full fat
    ▢ ¼ cup / 55g / 2 oz butter unsalted, softened
    ▢ 1.5-2 tablespoon powdered erythritol (recipe said: 5 tablespoons,6 tablespoon if you like a sweet cookie)
    ▢ 1 egg medium
    ▢ 2 cup / 200g almond flour use 2 tablespoons less for super-fine almond flour
    ▢ 4 tbsp lemon juice (circa 1 large lemon)
    ▢ zest of 1 lemon

Lemon Glaze

    ▢ ¼ cup / 30g powdered erythritol
    ▢ 1.5 tablespoon lemon juice
    ▢ lemon zest, to taste

Instructions
 
   Soften the butter
    Cream the butter, cream cheese, 4 tablespoon lemon juice and lemon zest together with the powdered erythritol until smooth. Use a food processor or an electric mixer.  No, we're trying with the lemon added after creaming
    Add the egg and mix until well-combined. Last, add the almond flour and mix.
    The dough is fairly soft at this stage. Wrap it in cling film and place either in the fridge for 20 - 30 minutes or in the freezer for about 10 minutes.
    Pre-heat the oven to 180 Celsius / 356 Fahrenheit.
    Line a baking tray with parchment paper. Form 12 cookie dough balls and place them on the tray, flattening them slightly with your hand.
    Bake for 15 minutes or until lightly browned at the bottom (and ever so slightly browned on top).
    Remove from the oven and let cool completely.
    Stir together the lemon juice, powdered erythritol and lemon zest for the glaze and spread over the cooled cookies. 
    We made part of the recipe with some chocolate powder...

# Quick Swedish keto hazelnut cookies
https://www.dietdoctor.com/recipes/quick-swedish-keto-hazelnut-cookies
Ingredients

    100 g butter, softened
    80 ml (70 g) erythritol
    1 egg yolk
    ½ tsp baking powder
    1 tsp vanilla extract
    ¼ tsp salt
    240 ml (110 g) almond flour
    240 ml (120 g) hazelnut flour
    18 hazelnuts

Instructions

    Preheat the oven to 300°F (150°C) and line a baking sheet with parchment paper.
    Mix the butter, sweetener, egg yolk, baking powder, vanilla extract, and salt in a bowl. Preferably with an electric mixer.
    Add the nut flours and mix until everything is combined and moist dough forms.
    Roll the dough with your hands into about 1" (2.5 cm) in diameter balls and spread them out on the baking sheet.
    Flatten them a bit with your fingers and gently press down a hazelnut in the middle of each cookie.
    Bake in the oven for about 15-20 minutes until golden. Let the cookies cool on the baking sheet for at least 30 minutes before removing them. Whilst hot the cookies will be really soft but will crisp up once they cool down.

Tip

Keeps fresh for at least one week in the fridge in an airtight container or for a couple of months in the freezer.

History: 2023-12-27: made em with erythritol.  too sweet???

# Keto no-bake gingerbread bites 
https://www.dietdoctor.com/recipes/keto-gingerbread-fatbombs

Ingredients

    110 g butter, softened
    300 ml (140 g) almond flour
    120 ml (65 g) powdered erythritol
    1 tsp ground cinnamon
    1 tsp ground ginger
    1 tsp ground cloves
    120 ml (70 g) roasted almonds, chopped or shaved

Instructions

    Combine all the ingredients, except the almonds, in a big bowl. Mix with your hands or in a food processor until a dough forms. If the dough is too soft, refrigerate for 20-30 or until it firms up.
    Use your hands to roll the dough into small balls, about 1" (2.5 cm) in diameter. Set aside.
    Roast the almonds in a dry frying pan on low heat until just browned and fragrant.
    Let the almonds cool to room temperature on a plate. When cool, roll the no-bake balls in the almonds until each ball is completely covered with almonds.
    Enjoy right away or refrigerate until serving.

Tip

You can also use granulated erythritol for this recipe but since this is a no-bake recipe you might get the crunchy texture from the erythritol which some people don't like.


# Chocolate Vanilla Cheesecake
 -8 servings
 -each: 4.9g CHO, 1.8 oz PRO

## Ingredients
 - 1 tsp butter
 - 1 Tbsp full-ft soy flour
 - 6 eggs, separated
 - 6 Equal tablets, or stevia powder to taste
 - 454g=1lb cream cheese
 - 1 cup sour cream
 - 6 drops vanilla extract
 - 0.25 cup cocoa powder

## Instructions
  -Preheat oven to 350F. 
  -Butter an 8 or 9" springform pan and dust with soy flour.
  -In large bowl, beat egg yolks with stevia until foamy. Add cream cheese, sour cream, and vanilla extract, and beat until fluffy.  In a separate bowl, beat egg whites untill stiff. fold into cream cheese mixture. pour half the mixture into the pan. mix coca powder into other half of mixture.  spoon that into the pan.
  -bake for 25-30 minutes or until golden brown

## History
cpbl 2021 10 30: i put about 3.5 drops of our stevia per serving.
cpbl 2021 12 26: i put 4 drops per servig

nutrition info does not include a crust: 
I also made a crust of 2 cups of almond flour plus 1/3 cup butter, melted. plus some vanilla and stevia. Bake for 10 minutes til lightly brown

# butter chicken
2022 01 27 tried https://www.castironketo.net/blog/keto-butter-chicken/




# Pancakes

https://lowcarbyum.com/fluffy-almond-meal-pancakes/

Ingredients

    ▢ 2 cups almond flour finely ground is best (see note)
    ▢ 4 eggs
    ▢ ½ cup water for puffier pancakes you can use sparkling water or add in a little vinegar (see note)
    ▢ ¼ cup oil
    ▢ 1 teaspoons baking soda use baking powder if you prefer
    ▢ ½ teaspoon salt
    ▢ 3-4 packets stevia or one tablespoon of granular sweetener

US Customary - Metric
Instructions

    Preheat griddle or pan over medium high heat (about 325-350°F).
    Mix ingredients together in a blender or bowl with electric mixer. Pour batter onto hot griddle.
    Flip them when the edges start to dry and tops are bubbling. Remove when both sides have browned.

Notes

    Makes about 24 small silver dollar size. Larger size can be made if desired. 
    If needed, the almond flour can be sifted up to 4 times to make the pancakes less grainy. 
    For fluffier pancakes, try adding a small amount of vinegar (up to 1 tablespoon) or use baking powder instead of soda.
    Another way to get fluffier pancakes is to add the egg yolks directly into the batter and beat the egg whites in a separate bowl. After the egg whites have reached soft peaks, gently fold them into the batter.
    Pancakes can be frozen and reheated in the microwave for about 20 seconds on high for each serving. Larger batches can be placed in oven at 350°F for about ten minutes.


# Keto Flourless Chocolate Cake

https://asweetlife.org/keto-flourless-chocolate-cake/

History:
2022-04-11: Tried this again. Maybe double-recipe to make cake thicker, or make two and fill with whipped cream.
2023-12-27: Made the two recipes together. The translucent white bowl is just big enough for eight whites.  Put >1/4 tsp of liquid stevia from Pure-le brand into yolks, plus 1 tsp of orange extract.  AND put 1 tsp of white powdered stevia from  Pure-le brand ("250 times sugar"). Forgot the vanilla; will put it in the filling...

## Ingredients

    6 ounces unsweetened chocolate, chopped = 170 g
    6 tbsp unsalted butter = 89 mL = 84 g
    4 large eggs, separated
    ¼ tsp salt
    ¼ tsp cream of tartar
    Powdered sweetener equivalent to 3/4 cup sugar, divided
    1 tsp vanilla extract
    2 tbsp warm water or coffee

Our instructions. We make double recipe and then put a cream cheese / etc filling in between them. We bake them in series since only have one pan. Nope, now we bought a second pan.

1. For making two recipes:
 - separate eggs into three bowls:
     - all egg yolks go into the largest glass mixing bowl    
     - one of the egg whites bowls shoud be the translucent white parallel-sided bowl. for the other, maybe a small metal bowl?
 - you can add salt and cream of tartar into whites right away

2. chocolate (and butter) is next critical path. melt (butter first) and let cool.  "In a heatproof bowl set over a pan of barely simmering water, combine the chocolate and butter. Stir until melted and smooth, then remove the bowl from the pan and let cool to lukewarm."

3. beat both sets of whites (2 minutes) to stiff peaks and add vanilla. "In a large (but not largest) bowl, beat the egg whites with the salt and cream of tartar until foamy. Slowly add ½ cup of the sweetener, beating continuously, until the mixture holds stiff peaks. Beat in the vanilla extract."   We skip the swerve/erythritol and instead, at the VERY end with the vanilla, beat in 1/2 tsp (???) of powdered stevia (2023-03)

4. then beat yolks in edge of large glass mixing bowl. Do them (two receipes) all together because this takes 4+ minutes. split in half.  " In a large bowl (big enough to receive egg whites), beat the egg yolks with the remaining sweetener for several minutes, until lightened and thickened. Slowly beat in (one recipe of) the melted chocolate. The mixture will become very thick. Beat in the water or coffee." 

5. then proceed with half chocolate, fold in whites, etc.

Full / original instructions:


  -   Preheat the oven 350F and grease a 9-inch springform pan. Line the bottom with parchment paper and grease the paper.
  -   
  -   
  -  
  -   Fold in the egg whites in three separate additions until no streaks remain. Spread the batter in the prepared baking pan and set the pan on a cookie sheet.
  -  Bake 30 to 40 minutes, until puffed and just barely firm to the touch. Remove and let cool completely in the pan, then run a sharp knife around the edges and loosen the sides.
  -  Serve with lightly (stevia/etc)-sweetened whipped cream, if desired.  Actually, we bake two and use fillings like that below.

## fillings for keto choc cake

https://www.dietdoctor.com/recipes/no-bake-keto-cheesecake-with-toppings

# Bread and buns

This one rises beautifully in a single go (using rapid yeast), slices thinly, toasts nicely, etc, and it uses gluten flax, oat fiber, and xanthan gum, all of which we have, and just like the pre-mixed product by "Keto 1.0". It's also easier to make than another with similar ingedients:

https://www.supergoldenbakes.com/keto-bread-recipe/#recipe

I sliced 22 thin slices from one loaf (rather than the 18 thin slices they suggest).  I sliced 36 thin slices another time!!

ngredients

    ▢ 1 ¼ cup (185g) Vital Wheat Gluten
    ▢ ⅔ cup (80g) Golden Milled Flaxseed
    ▢ ½ cup (30g) oat fiber or potato fiber
    ▢ 3 tbsp powdered Swerve or other Keto Sugar Substitute
    ▢ 3 tsp rapid rise yeast
    ▢ 1 tsp salt
    ▢ ¼ tsp Xantham Gum
    ▢ 1 cup (240ml) water body temperature
    ▢ 2 eggs lightly beaten
    ▢ 2 tbsp softened ghee or grass fed butter
    ▢ 2 tbsp heavy cream to brush on bread
    ▢ 3 tbsp mixed seeds optional

Instructions

    Put the vital wheat gluten, oat fiber, golden milled flaxseed, sweetener and yeast in a bowl and stir to combine.
    Add the eggs, butter and water. Use a hand mixer fitted with dough beaters (or a stand mixer) to mix the dough on low speed initially. Sprinkle with the salt and Xanthan Gum.
    Increase the speed and keep mixing until the dough comes together and clumps around the beaters.
    Tip the dough onto your worktop and knead together briefly. Stretch the dough to form a rectangle that’s the same size as the long edge of your loaf tin.
    Roll the dough as you would a Swiss Roll and place in the tin. Cover loosely with a plastic bag and place it in a warm spot to rise for 1 ½ – 2 hours. The dough will expand to fill the tin and rise over the top of it.
    Preheat the oven to 180C (350F). Brush the loaf with the cream and add seeds or any other toppings you like.
    Bake for 30-35 minutes, or until the bread is golden and well risen. You can add some ice cubes on the bottom of the oven to help the bread rise.
    Lift the bread out of the tin and allow it to cool completely before slicing.



Nutritional Info
Calories: 115kcal | Carbohydrates: 7g | Protein: 10g | Fat: 7g | Saturated Fat: 2g | Cholesterol: 27mg | Sodium: 173mg | Potassium: 108mg | Fiber: 4g | Sugar: 1g | Vitamin A: 101IU | Calcium: 36mg | Iron: 1mg


# Mayonnaise

    1 whole egg
    ½ tbsp lemon juice
    1 tsp white wine vinegar
    ¼ tsp dijon mustard
    ¼ tsp sea salt
    1 cup avocado oil, or light-flavored olive oil

Instructions 
(2022: we used 100% avocado!)

    Add all of the ingredients (with the oil last) into the jar that came with your stick blender.
    Give the ingredients a minute to settle, with the oil separating on top.
    Place your stick blender in the jar and press it firmly to the bottom. Turn it on and keep it pressed against the bottom of the jar for at least 10-15 seconds. Once the mayonnaise starts to emulsify and thicken, slowly move the stick blender up and down to fully combine the ingredients.
    Once it's all blended, remove the stick blender. Give it a few stirs with a spoon and place in a storage container in the refrigerator. It will stay fresh for up to one week.

    Troubleshooting
https://downshiftology.com/how-to-make-homemade-mayonnaise/
Why not to use extra virgin olive oil (maybe find some light olive oil instead)
https://www.seriouseats.com/two-minute-mayonnaise
