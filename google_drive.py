#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import re



def downloadOpenGoogleDoc(url, filename=None, fileformat=None, pandas=False, update=True):
    """
    If you: Set a Google doc or Google drive document to be readable (avoid editable, do avoid downloading comments) to anyone with the link:

    Then this function will return a version of it from online, if networked, or the latest download, if not.

    The filename (currently mandatory; should be fixed to be a hash of the url by default) should not include a path.

    Specify fileformat as one of "txt", "ods", "xlsx", "odt" or etc as supported by Google

    This returns the full-path filename of the downloaded file, unless pandas is True for spreadsheets, in which case it returns a pandas version.

    If update False, and the file has already been downloaded, download will be skipped.

    N.B.: For txt file download, File should be set to anyone with the link can view, but not comment. Otherwise comments will also be downloaded to a text file.

    """
    from  xlrd import XLRDError
    if pandas:
        import pandas as pd
        #assert fileformat in ['xlsx']

    if not url.endswith('/'): url=url+'/'
    if fileformat is None:
        fileformat='ods' #'xlsx'

    full_file_name        =filename
    if update or not os.path.exists(full_file_name):
        oss=' wget "'+url+'export?format='+fileformat+'" -O '+full_file_name+'-dl'
        print(oss)
        result=os.system(oss)
        if result:
            print('  NO INTERNET CONNECTION, or other problem ({}) grabbing Google Docs file. Using old offline version ({})...'.format(result,filename))
        else:
            if fileformat in ['txt']:
                result = os.system('dos2unix -n '+ full_file_name+'-dl ' + full_file_name)
            else:
                result=os.system(' mv '+ full_file_name+'-dl ' + full_file_name)
                if 0:   # This doesn't work anymore. read() not willing to read a binary file if it doesn't look like utf8. So need more try/except here
                    checkt = open(full_file_name, 'rb').read()
                    if checkt.startswith('<!DOCTYPE html>'):
                        raise TypeError("Downloaded file arrived as HTML; this means something's wrong")
            print(' ... Downloaded and wrote {}'.format(full_file_name))
            assert not result
    else:
        print(('   Using local (offline) version of '+filename))
    if pandas and fileformat in ['xlsx']:
        return(   pd.read_excel(full_file_name)  )
    if pandas and fileformat in ['ods']:
        return(   pd.read_excel(full_file_name,  engine='odf'))


    return(full_file_name)

